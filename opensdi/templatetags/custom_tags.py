from django import template
from django.templatetags.static import static
import opensdi.settings as settings
import requests


register = template.Library()


@register.simple_tag
def get_campaign_image():
    return static('img/campaign/' + settings.CAMPAIGN_IMAGE_NAME)
    

@register.simple_tag
def get_campaign_name():
    return settings.CAMPAIGN_TITLE
